import pandas as pd
import numpy as np
from scipy.optimize import minimize


class SmartBeta:

    def __init__(self, data: pd.DataFrame, covariance_adjustment=None):
        self._data = data.copy().dropna()

        if covariance_adjustment is None:
            self._covariance = self._data.cov()

    def _global_minimum_variance_function(self, weights):
        return np.dot(np.dot(weights, self._covariance), weights)

    def _negative_maximum_sharpe_ratio_function(self, weights, mu, risk_free_rate):
        return -(np.dot(mu, weights) - risk_free_rate) / ((np.dot(np.dot(weights, self._covariance), weights)) ** (1/2))

    def _negative_maximum_diversification_ratio_function(self, weights):
        variance = np.diag(self._covariance)
        temp = np.dot(variance**(1/2), weights)
        return -temp/(np.dot(np.dot(weights, self._covariance), weights) ** (1/2))

    def fetch_global_minimum_variance_weightings(self, max_individual_holding=0.1, long_only=True):
        n = self._covariance.shape[0]
        weights = np.ones(n)/n
        constraints = ({'type': 'eq', 'fun': lambda x: 1 - sum(x)})
        bounds = [(0, max_individual_holding) for i in weights]

        if long_only:
            res = minimize(self._global_minimum_variance_function, x0=weights,
                           method='SLSQP', constraints=constraints, bounds=bounds, tol=1e-30)
        else:
            res = minimize(self._global_minimum_variance_function, x0=weights,
                           method='SLSQP', constraints=constraints, tol=1e-30)

        return res.x

    def fetch_maximum_sharpe_ratio_weightings(self, max_individual_holding=0.1, long_only=True,
                                              risk_free_rate=0.0, mean_method='arithmetic_average'):
        n = self._covariance.shape[0]
        weights = np.ones(n)/n
        constraints = ({'type': 'eq', 'fun': lambda x: 1 - sum(x)})
        bounds = [(0, max_individual_holding) for i in weights]

        # TODO: Add situations that mean_method calculation is NOT arithmetic average.
        mu = self._data.mean()
        if mean_method == 'arithmetic_average':
            mu = self._data.mean()

        if long_only:
            res = minimize(self._negative_maximum_sharpe_ratio_function, x0=weights, args=(mu, risk_free_rate),
                           method='SLSQP', constraints=constraints, bounds=bounds, tol=1e-30)
        else:
            res = minimize(self._negative_maximum_sharpe_ratio_function, x0=weights, args=(mu, risk_free_rate),
                           method='SLSQP', constraints=constraints, tol=1e-30)

        return res.x

    def fetch_maximum_diversification_ratio_weightings(self, max_individual_holding=0.1, long_only=True):
        n = self._covariance.shape[0]
        weights = np.ones(n)/n
        constraints = ({'type': 'eq', 'fun': lambda x: 1 - sum(x)})
        bounds = [(0, max_individual_holding) for i in weights]

        if long_only:
            res = minimize(self._negative_maximum_diversification_ratio_function, x0=weights,
                           method='SLSQP', constraints=constraints, bounds=bounds, tol=1e-30)
        else:
            res = minimize(self._negative_maximum_diversification_ratio_function, x0=weights,
                           method='SLSQP', constraints=constraints, tol=1e-30)

        return res.x
