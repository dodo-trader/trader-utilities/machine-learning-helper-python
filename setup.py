from setuptools import setup, find_packages

version = {}
with open("ml_helper/version.py") as file:
    exec(file.read(), version)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="mlhelperpy",
    version=version['__version__'],
    author="Ernest Yuen",
    author_email="ernestyuen08@gmail.com",
    description="Machine Learning Helper",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/dodo-trader/trader-utilities/machine-learning-helper-python",
    packages=find_packages(),
    install_requires=[
        "numpy==1.19.3",
        "pandas==1.1.4",
        "python-dateutil==2.8.1",
        "pytz==2020.4",
        "six==1.15.0",
        "scipy==1.5.4"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
